#  PrinterTestApp
[![js-standard-style](https://img.shields.io/badge/code%20style-standard-brightgreen.svg?style=flat)](http://standardjs.com/)

* Standard compliant React Native App

## :arrow_up: How to Setup

**Step 1:** git clone this repo:

**Step 2:** cd to the cloned repo:

**Step 3:** Install the Application with `yarn` or `npm i`

**Step 4:** Link native dependencies with `react-native link`

**Step 4:** Copy StarIO SDK framework files: SMCloudServices.framework, StarIO.framework and StarIO_Extension.framework to the ios directory
