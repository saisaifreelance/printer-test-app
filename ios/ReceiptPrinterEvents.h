//
//  ReceiptPrinterEvents.h
//  PrinterTestApp
//
//  Created by Jabulani Mpofu on 7/3/2018.
//  Copyright © 2018 Facebook. All rights reserved.
//

#ifndef ReceiptPrinterEvents_h
#define ReceiptPrinterEvents_h

#import <Foundation/Foundation.h>

static NSString *const PRINTER_LIST_CHANGED_EVENT = @"printer_list_changed";
static NSString *const CONNECTED_PRINTER_CHANGED_EVENT = @"connected_printer_changed";
static NSString *const PRINTER_STATUS_CHANGED_EVENT = @"printer_status_changed";

#endif /* ReceiptPrinterEvents_h */
