//
//  PrintData.h
//  PrinterTestApp
//
//  Created by Jabulani Mpofu on 24/2/2018.
//  Copyright © 2018 Facebook. All rights reserved.
//

#ifndef PrintData_h
#define PrintData_h


#endif /* PrintData_h */

#import <Foundation/Foundation.h>

@interface PrintData : NSObject

@property (nonatomic, strong) NSDictionary *dictionary;
@property (nonatomic, strong) NSString *filePath;

- (id)initWithDictionary:(NSDictionary *)dictionary atFilePath:(NSString *)filePath;

@end
