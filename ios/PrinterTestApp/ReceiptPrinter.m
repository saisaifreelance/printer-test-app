//
//  ReceiptPrinter.m
//  PrinterTestApp
//
//  Created by Jabulani Mpofu on 23/2/2018.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <StarIO/SMPort.h>
#import "ReceiptPrinter.h"
#import "React/RCTLog.h"

#define DEBUG_LOGGING           YES
#define DEBUG_PREFIX            @"ReceiptPrinter:"
@interface ReceiptPrinter ()

@property (nonatomic, assign) BOOL searching;
@property (nonatomic, assign) BOOL empty;

@property (nonatomic, strong) NSMutableArray *printers;
@property (nonatomic, strong) Printer *connectedPrinter;
@property (nonatomic, assign) PrinterStatus printerStatus;

@property (nonatomic, strong) NSHashTable *delegates;

@end

@implementation ReceiptPrinter

#pragma mark - Delegates

- (void)addDelegate:(id<PrinterConnectivityDelegate>)delegate
{
  [self log:@"Add DELEGATE!"];
  [_delegates addObject:delegate];
}

- (void)removeDelegate:(id<PrinterConnectivityDelegate>)delegate
{
  [self log:@"Remove DELEGATE!"];
  [_delegates removeObject:delegate];
}

- (void)notifyDelegates
{
  [self log:@"Notify DELEGATES!"];
  for (id <PrinterConnectivityDelegate> d in _delegates) {
    [self log: [NSString stringWithFormat: @"DELEGATE: %@", d]];
    [d connectedPrinterDidChangeTo:self.connectedPrinter];
  }
}

- (void)printer:(Printer *)printer didChangeStatus:(PrinterStatus)status
{
  [self log:@("Printer did change status - %s!")];
  if([_printers containsObject:printer]) {
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[_printers indexOfObject:printer] inSection:0];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *encoded = [NSKeyedArchiver archivedDataWithRootObject:self.connectedPrinter];
    [defaults setObject:encoded forKey:kConnectedPrinterKey];
    [defaults synchronize];
    
    _printerStatus = status;
  }
}

#pragma mark - utils

- (void)log:(NSString *)message
{
  if(DEBUG_LOGGING) {
    NSLog(@"%@", [NSString stringWithFormat:@"%@ %@ -> %@", DEBUG_PREFIX, self, message]);
    RCTLogInfo(@"%@", [NSString stringWithFormat:@"RCTLOG>%@ %@ -> %@", DEBUG_PREFIX, self, message]);
  }
}

- (CGFloat)widthOfString:(NSString *)string withFont:(NSFont *)font {
  NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:font, NSFontAttributeName, nil];
  return [[[NSAttributedString alloc] initWithString:string attributes:attributes] size].width;
}

#pragma mark - printable

- (PrintData *)printedFormat {
  NSString *filePath = [[NSBundle mainBundle] pathForResource:@"example" ofType:@"xml"];
  
  NSDictionary *dictionary = @{
                               @"{{userText}}" : @"Some text"
                               };
  
  return [[PrintData alloc] initWithDictionary:dictionary atFilePath:filePath];
}

- (void)print {
  
}

- (void)print:(Printer *)printer {
  
}

#pragma mark - RCTMethods

RCT_EXPORT_MODULE(ReceiptPrinter);

- (id)init {
  self = [super init];
  if (self != nil) {
    [self log: @"Setting up ReceiptPrinter instance"];
    self.printers = [NSMutableArray array];
    if ([Printer connectedPrinter]) {
      [self.printers addObject:[Printer connectedPrinter]];
    }
    self.delegates = [NSHashTable weakObjectsHashTable];
  }
  return self;
}

- (NSArray<NSString *> *)supportedEvents {
  return @[];
}

/**
 * React native constant exports
 * @return NSDictionary
 */
- (NSDictionary *)constantsToExport {
  NSMutableDictionary *constants = [NSMutableDictionary new];
  
  constants[@"demo"] = @"demo";
  return constants;
}

/**
 addPrinterStateListener
 */
RCT_EXPORT_METHOD(addPrinterStateListener) {
  
}

/**
 addPrinterStateListener
 */
RCT_EXPORT_METHOD(removePrinterStateListener) {
  
}

/**
 addPrinterStateListener
 */
RCT_EXPORT_METHOD(addPrintersListener) {
  
}

/**
 addPrinterStateListener
 */
RCT_EXPORT_METHOD(removePrintersListener) {
  
}

RCT_REMAP_METHOD(search,
                 resolver:(RCTPromiseResolveBlock)resolve
                 rejecter:(RCTPromiseRejectBlock)reject)
{
  if(_searching) return;
  
  [_printers removeAllObjects];
  
  self.searching = YES;
  self.connectedPrinter = nil;
  NSMutableArray *results = [NSMutableArray array];
  
  [Printer search:^(NSArray *found) {
    if([found count] > 0) {
      [self log: @"Printer search"];
      [self log: [NSString stringWithFormat: @"%@", found]];
      [_printers addObjectsFromArray:found];
      [_printers sortUsingComparator:^NSComparisonResult(Printer *obj1, Printer *obj2) {
        return obj1.isCompatible ? NSOrderedAscending : NSOrderedDescending;
      }];
      
      for(Printer *p in found) {
        [self log: @"Found printer!"];
        NSMutableDictionary *printerDictionary = [[p dictionaryValue] mutableCopy];
        [results addObject:  printerDictionary];
      }
      
      if(!_connectedPrinter) {
        Printer *lastKnownPrinter = [Printer connectedPrinter];
        for(Printer *p in found) {
          if([p.macAddress isEqualToString:lastKnownPrinter.macAddress]) {
            self.connectedPrinter = p;
            break;
          }
        }
      }
    }
    
    self.empty = [found count] == 0;
    self.searching = NO;
    
    if (self.empty) {
      NSError *underlyingError = [[NSError alloc] initWithDomain:@"search"
                                                            code:0
                                                        userInfo:@{@"message": @"Could not find any printers near you"}];
      [self promiseRejectException:reject error:underlyingError];
    } else {
      resolve(results);
    }
  }];
}

// todo refactor from calling print methods directly and prefer to use the Printable protocol
RCT_REMAP_METHOD(setConnectedPrinter,
                 printerData:(NSDictionary *)printerData
                 heartBeat:(BOOL)heartBeat
                 set_printer_resolver:(RCTPromiseResolveBlock)resolve
                 set_printer_rejecter:(RCTPromiseRejectBlock)reject)
{
  [self log: @"SetConnectedPrinter_ called"];
  NSString *macAddress = [printerData objectForKey:@"macAddress"];
  Printer *printer = nil;
  
  if ([_connectedPrinter.macAddress isEqualToString:macAddress]) {
    resolve([_connectedPrinter dictionaryValue]);
    return;
  } else if ([_printers count] > 0) {
    [self log: @"SetConnectedPrinter_: have printers found, let's use on of the found printers"];
    for(Printer *p in _printers) {
      if([p.macAddress isEqualToString:macAddress]) {
        printer = p;
        break;
      }
    }
  } else {
    [self log: @"SetConnectedPrinter_: no printers are stored"];
    NSError *underlyingError = [[NSError alloc] initWithDomain:@"connect"
                                                          code:1
                                                      userInfo:@{
                                                                 @"message": @"No current found printers"
                                                                 }];
    [self promiseRejectException:reject error:underlyingError];
    return;
  }
  
  [self log: @"SetConnectedPrinter_ start"];
  [self log: [NSString stringWithFormat:@"SetConnectedPrinter_ start: Printer: %@", [printer dictionaryValue]]];
  
  if(printer == nil && _connectedPrinter) {
    [self log: @"SetConnectedPrinter_: nil"];
    if([_connectedPrinter isReadyToPrint]) {
      [_connectedPrinter disconnect];
    }
    _connectedPrinter = nil;
    [self notifyDelegates];
  } else if(printer) {
    [self log: @"SetConnectedPrinter_: not nil"];
    _connectedPrinter = printer;
    _connectedPrinter.delegate = self;
    [self log: @"SetConnectedPrinter_: connect"];
    [_connectedPrinter connect:^(BOOL success) {
      [self log: [NSString stringWithFormat:@"SetConnectedPrinter_: connect response: %@", success ? @"true" : @"false"] ];
      if(!success){
        self.connectedPrinter = nil;
      }
      
      [self log: @"SetConnectedPrinter_: still working"];
      [self notifyDelegates];
      resolve([printer dictionaryValue]);
    }];
  }
}

// todo refactor from calling print methods directly and prefer to use the Printable protocol
RCT_REMAP_METHOD(disconnectPrinter,
                 disconnect_printer_resolver:(RCTPromiseResolveBlock)resolve
                 disconnect_printer_rejecter:(RCTPromiseRejectBlock)reject)
{
  [self log: @"DisonnectPrinter_ called"];
  Printer *printer = [Printer connectedPrinter];
  if(printer) {
    [self log: @"DisonnectPrinter_: we have a printer to disconnect"];
    [printer disconnect];
  } else {
    [self log: @"DisonnectPrinter_: no printers are stored"];
  }
  
  [self log: @"DisonnectPrinter_: still working"];
  [self notifyDelegates];
  resolve([printer dictionaryValue]);
}


RCT_REMAP_METHOD(getPrinters,
                 getPrintersResolver:(RCTPromiseResolveBlock)resolve
                 getPrintersRejecter:(RCTPromiseRejectBlock)reject)
{
  if (_empty) {
    NSError *underlyingError = [[NSError alloc] initWithDomain:@"printer"
                                                          code:3
                                                      userInfo:@{
                                                                 @"message": @"No current found printers"
                                                                 }];
    [self promiseRejectException:reject error:underlyingError];
    return;
  }
  NSMutableArray *results = [NSMutableArray array];
  for(Printer *p in _printers) {
    NSMutableDictionary *printerDictionary = [[p dictionaryValue] mutableCopy];
    [results addObject:  printerDictionary];
  }
  resolve(results);
}

RCT_REMAP_METHOD(getConnectedPrinter,
                 getConnectedPrinterResolver:(RCTPromiseResolveBlock)resolve
                 getConnectedPrinterRejecter:(RCTPromiseRejectBlock)reject)
{
  if (_empty || _connectedPrinter == nil) {
    NSError *underlyingError = [[NSError alloc] initWithDomain:@"printer"
                                                          code:4
                                                      userInfo:@{
                                                                 @"message": @"No connected printer"
                                                                 }];
    [self promiseRejectException:reject error:underlyingError];
    return;
  }
  resolve([_connectedPrinter dictionaryValue]);
}

RCT_REMAP_METHOD(print,
                 dictionary:(NSDictionary *)dictionary
                 template:(NSString *)template
                 printResolver:(RCTPromiseResolveBlock)resolve
                 printRejecter:(RCTPromiseRejectBlock)reject)
{
  Printer *printer = [Printer connectedPrinter];
  if(!printer) {
    NSError *underlyingError = [[NSError alloc] initWithDomain:@"print"
                                                          code:1
                                                      userInfo:@{
                                                                 @"message": @"No connected printer found"
                                                                 }];
    [self promiseRejectException:reject error:underlyingError];
    return;
  }
  
  NSMutableString *xml = [[NSMutableString alloc] initWithString:template];
  [[Printer connectedPrinter] printContent:dictionary xml:xml];
  
  resolve(self.printers);
}

#pragma mark - Promises and Errors

/**
 Reject a promise with an exception
 @param reject RCTPromiseRejectBlock
 @param error NSError
 */
- (void)promiseRejectException:(RCTPromiseRejectBlock)reject error:(NSError *)error {
  NSDictionary * jsError = [self getJSError:(error)];
  reject([jsError valueForKey:@"code"], [jsError valueForKey:@"message"], error);
}

/**
 Reject a promise with an exception
 @param error NSError
 */
- (NSDictionary *)getJSError:(NSError *)error {
  NSString *code = @"printer/unknown";
  NSString *message = [error localizedDescription];
  NSString *nativeErrorMessage = [error localizedDescription];
  
  switch (error.code) {
    case 0:
      code = @"printer/no-printer-found";
      message = @"Could not find any printers near you";
      break;
    default:
      break;
  }
  
  return @{
           @"code": code,
           @"message": message,
           @"nativeErrorMessage": nativeErrorMessage,
           };
}

@end
