//
//  ReceiptPrinterUtil.h
//  PrinterTestApp
//
//  Created by Jabulani Mpofu on 7/3/2018.
//  Copyright © 2018 Facebook. All rights reserved.
//

#ifndef ReceiptPrinterUtil_h
#define ReceiptPrinterUtil_h

#import <Foundation/Foundation.h>
#import <React/RCTEventEmitter.h>

@interface ReceiptPrinterUtil : NSObject

+ (void)sendJSEvent:(RCTEventEmitter *)emitter name:(NSString *)name body:(NSDictionary *)body;

@end

#endif /* ReceiptPrinterUtil_h */
