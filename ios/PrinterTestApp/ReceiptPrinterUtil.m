//
//  ReceiptPrinterUtil.m
//  PrinterTestApp
//
//  Created by Jabulani Mpofu on 7/3/2018.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ReceiptPrinterUtil.h"

@implementation ReceiptPrinterUtil

+ (void)sendJSEvent:(RCTEventEmitter *)emitter name:(NSString *)name body:(NSDictionary *)body {
  @try {
    // TODO: Temporary fix
    // until a better solution comes around
    if (emitter.bridge) {
      [emitter sendEventWithName:name body:body];
    }
  } @catch (NSException *error) {
    NSLog(@"An error occurred in sendJSEvent: %@", [error debugDescription]);
  }
}

@end
