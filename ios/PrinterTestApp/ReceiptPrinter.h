//
//  ReceiptPrinter.h
//  PrinterTestApp
//
//  Created by Jabulani Mpofu on 23/2/2018.
//  Copyright © 2018 Facebook. All rights reserved.
//

#ifndef ReceiptPrinter_h
#define ReceiptPrinter_h


#endif /* ReceiptPrinter_h */

#import <StarIO/SMPort.h>
#import <Foundation/Foundation.h>
#import <React/RCTEventEmitter.h>
#import <React/RCTBridgeModule.h>
#import "Printer.h"
#import "Printable.h"
#import "PrintParser.h"
#import "PrintData.h"

@protocol PrinterConnectivityDelegate <NSObject>

- (void)connectedPrinterDidChangeTo:(Printer *)printer;

@end

@class Printer,GlobalNavigationViewController;
@interface ReceiptPrinter : RCTEventEmitter <RCTBridgeModule, Printable, PrinterDelegate>

- (void)addDelegate:(id<PrinterConnectivityDelegate>)delegate;
- (void)removeDelegate:(id<PrinterConnectivityDelegate>)delegate;

+ (NSString *)iconForPrinterStatus:(PrinterStatus)status;
+ (NSString *)statusMessageForPrinterStatus:(PrinterStatus)status;

@end
