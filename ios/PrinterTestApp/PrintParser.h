//
//  PrintParser.h
//  PrinterTestApp
//
//  Created by Jabulani Mpofu on 25/2/2018.
//  Copyright © 2018 Facebook. All rights reserved.
//

#ifndef PrintParser_h
#define PrintParser_h


#endif /* PrintParser_h */

#import <Foundation/Foundation.h>
#import "ModelCapability.h"

@interface PrintParser : NSObject <NSXMLParserDelegate>

- (NSData *)parse:(NSData *)data;
- (PrintParser *)parse:(NSData *)data emulation:(StarIoExtEmulation)emulation;

@end
