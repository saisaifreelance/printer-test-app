//
//  PrintTextFormatter.h
//  PrinterTestApp
//
//  Created by Jabulani Mpofu on 25/2/2018.
//  Copyright © 2018 Facebook. All rights reserved.
//

#ifndef PrintTextFormatter_h
#define PrintTextFormatter_h


#endif /* PrintTextFormatter_h */

#import <Foundation/Foundation.h>
#import <StarIO_Extension/StarIoExt.h>
#import "PrinterCommands.h"

typedef NSString *(^PrintTextFormatterBlock)(NSString *text);

typedef NS_ENUM(NSInteger, PaperSizeIndex) {
  PaperSizeIndexTwoInch = 384,
  PaperSizeIndexThreeInch = 576,
  PaperSizeIndexFourInch = 832,
  PaperSizeIndexEscPosThreeInch = 512,
  PaperSizeIndexDotImpactThreeInch = 210
};

@interface PrintTextFormatter : NSObject

@property (nonatomic, readonly) NSData *formattedData;

+ (PrintTextFormatter *)formatter;
+ (PrintTextFormatter *)formatter:(StarIoExtEmulation)emulation;

// Add a manual command
- (void)add:(NSString *)command;

// Commands
- (void)tab;
- (void)newline;
- (void)dashedNewLine;

// Text Formatting
- (void)bold:(NSString *)text next:(PrintTextFormatterBlock)block;
- (void)bold:(BOOL)start;
- (void)underline:(NSString *)text next:(PrintTextFormatterBlock)block;
- (void)underline:(BOOL)start;
- (void)upperline:(NSString *)text next:(PrintTextFormatterBlock)block;
- (void)upperline:(BOOL)start;
- (void)scale:(NSString *)text width:(int)width height:(int)height next:(PrintTextFormatterBlock)block;
- (void)scale:(BOOL)start width:(int)width height:(int)height;
- (void)invertColor:(NSString *)text next:(PrintTextFormatterBlock)block;
- (void)invertColor:(BOOL)start;
- (void)rows:(int)col1 col2:(int)col2;
- (void)row:(NSString *)col1 col2:(NSString *)col2;
- (void)image:(NSString *)url width:(int)width height:(int)height;

// Text alignment
- (void)alignLeft;
- (void)alignRight;
- (void)alignCenter;

// Barcode
- (void)barcode:(PrinterBarcodeType)type text:(NSString *)text;

@end
