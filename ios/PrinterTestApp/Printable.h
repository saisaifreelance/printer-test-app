//
//  Printable.h
//  PrinterTestApp
//
//  Created by Jabulani Mpofu on 25/2/2018.
//  Copyright © 2018 Facebook. All rights reserved.
//

#ifndef Printable_h
#define Printable_h


#endif /* Printable_h */

#import <Foundation/Foundation.h>
#import "Printer.h"
#import "PrintData.h"

@protocol Printable <NSObject>

@required

- (PrintData *)printedFormat;

@end

@interface NSObject (Printable)

- (void)print;
- (void)print:(Printer *)printer;

@end
