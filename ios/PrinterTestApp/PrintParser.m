//
//  PrintParser.m
//  PrinterTestApp
//
//  Created by Jabulani Mpofu on 25/2/2018.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import "PrintParser.h"
#import "PrintTextFormatter.h"
#import "PrinterCommands.h"

typedef enum PrintFormatElementType
{
  PrintFormatElementTypeText,
  PrintFormatElementTypeBold,
  PrintFormatElementTypeUnderline,
  PrintFormatElementTypeUpperline,
  PrintFormatElementTypeDashedline,
  PrintFormatElementTypeNewline,
  PrintFormatElementTypeTab,
  PrintFormatElementTypeScale,
  PrintFormatElementTypeInvertColor,
  PrintFormatElementTypeUnknown,
  PrintFormatElementTypeAlignCenter,
  PrintFormatElementTypeAlignLeft,
  PrintFormatElementTypeAlignRight,
  PrintFormatElementTypeBarcode,
  PrintFormatElementTypeRows,
  PrintFormatElementTypeRow,
  PrintFormatElementTypeCut,
  PrintFormatElementTypeImage,
} PrintFormatElementType;


@interface PrintParser ()

@property (nonatomic, strong) PrintTextFormatter *formatter;
@property (nonatomic, assign) PrintFormatElementType currentElementType;

- (NSArray *)elementNamesForFormatterType:(PrintFormatElementType)type;
- (PrintFormatElementType)elementTypeForName:(NSString *)name;

@end

@implementation PrintParser

#pragma mark - Parsing

- (NSData *)parse:(NSData *)data
{
  self.formatter = [PrintTextFormatter formatter];
  
  NSXMLParser *parser = [[NSXMLParser alloc] initWithData:data];
  parser.delegate = self;
  [parser parse];
  
  return _formatter.formattedData;
}

- (NSData *)parse:(NSData *)data emulation:(StarIoExtEmulation)emulation
{
  self.formatter = [PrintTextFormatter formatter:emulation];
  
  NSXMLParser *parser = [[NSXMLParser alloc] initWithData:data];
  parser.delegate = self;
//  [parser parse];
  
  return _formatter.formattedData;
}

#pragma mark - NSXMLParserDelegate

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
  PrintFormatElementType elementType =  self.currentElementType = [self elementTypeForName:elementName];
  
  switch (elementType) {
    case PrintFormatElementTypeNewline:
      [_formatter newline];
      break;
    case PrintFormatElementTypeDashedline:
      [_formatter dashedNewLine];
      break;
    case PrintFormatElementTypeTab:
      [_formatter tab];
      break;
    case PrintFormatElementTypeBold:
      [_formatter bold:YES];
      break;
    case PrintFormatElementTypeInvertColor:
      [_formatter invertColor:YES];
      break;
    case PrintFormatElementTypeScale: {
      NSString * width = [attributeDict valueForKey:@"width"];
      NSString * height = [attributeDict valueForKey:@"height"];
      NSString * scale = [attributeDict valueForKey:@"scale"];
      
      int w = 2;
      int h = 2;
      if ([scale intValue]) {
        w = h = [scale intValue];
      } else {
        if ([width intValue]) {
          w = [width intValue];
        }
        if ([height intValue]) {
          h = [height intValue];
        }
      }
      
      [_formatter scale:YES width:w height:h];
    }
      break;
    case PrintFormatElementTypeUnderline:
      [_formatter underline:YES];
      break;
    case PrintFormatElementTypeUpperline:
      [_formatter upperline:YES];
      break;
    case PrintFormatElementTypeAlignCenter:
      [_formatter alignCenter];
      break;
    case PrintFormatElementTypeAlignLeft:
      [_formatter alignLeft];
      break;
    case PrintFormatElementTypeAlignRight:
      [_formatter alignRight];
      break;
    case PrintFormatElementTypeBarcode: {
      NSString *str = @"account";
    }
      break;
    case PrintFormatElementTypeRows: {
      NSString * str = [attributeDict valueForKey:@"col1"];
      NSString * str2 = [attributeDict valueForKey:@"col2"];
      NSString * size = [attributeDict valueForKey:@"size"];
      [_formatter rows:[str intValue] col2:[str2 intValue]];
    }
      break;
    case PrintFormatElementTypeRow: {
      NSString * str = [attributeDict valueForKey:@"col1"];
      NSString * str2 = [attributeDict valueForKey:@"col2"];
      [_formatter row:str col2:str2];
    }
      break;
    case PrintFormatElementTypeImage: {
      NSString * source = [attributeDict valueForKey:@"source"];
      NSString * width = [attributeDict valueForKey:@"width"];
      NSString * height = [attributeDict valueForKey:@"height"];
      [_formatter image:source width:[width intValue] height:[height intValue]];
    }
      break;
    default:
      break;
  }
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
  PrintFormatElementType elementType = [self elementTypeForName:elementName];
  
  switch (elementType) {
    case PrintFormatElementTypeBold:
      [_formatter bold:NO];
      break;
    case PrintFormatElementTypeInvertColor:
      [_formatter invertColor:NO];
      break;
    case PrintFormatElementTypeScale:
      [_formatter scale:NO width:1 height:1];
      break;
    case PrintFormatElementTypeUnderline:
      [_formatter underline:NO];
      break;
    case PrintFormatElementTypeUpperline:
      [_formatter upperline:NO];
      break;
    default:
      break;
  }
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
  if([[string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
    
    NSMutableString *text = [NSMutableString stringWithString:string];
    [text replaceOccurrencesOfString:@"\\t" withString:kPrinterCMD_Tab options:0 range:NSMakeRange(0, [string length])];
    [text replaceOccurrencesOfString:@"\\n" withString:kPrinterCMD_Newline options:0 range:NSMakeRange(0, [text length])];
    
    if(_currentElementType == PrintFormatElementTypeBarcode) {
      [_formatter barcode:PrinterBarcodeTypeCode128 text:text];
    } else {
      [_formatter add:text];
    }
  }
}

- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError
{
  NSLog(@"Parser Error: %@",parseError);
}

- (void)parser:(NSXMLParser *)parser validationErrorOccurred:(NSError *)validationError
{
  NSLog(@"Parser Validation Error: %@",validationError);
}

#pragma mark - Helpers

- (NSArray *)elementNamesForFormatterType:(PrintFormatElementType)type
{
  switch (type) {
    case PrintFormatElementTypeText:
      return @[@"text", @"t"];
      break;
    case PrintFormatElementTypeBold:
      return @[@"bold", @"b"];
      break;
    case PrintFormatElementTypeUnderline:
      return @[@"underline", @"ul"];
      break;
    case PrintFormatElementTypeUpperline:
      return @[@"upperline", @"upl"];;
      break;
    case PrintFormatElementTypeDashedline:
      return @[@"dashednewline", @"dnl", @"dl"];
      break;
    case PrintFormatElementTypeTab:
      return @[@"tab", @"tb"];
      break;
    case PrintFormatElementTypeNewline:
      return @[@"newline", @"nl"];
      break;
    case PrintFormatElementTypeScale:
      return @[@"scale", @"sc"];
      break;
    case PrintFormatElementTypeInvertColor:
      return @[@"invertcolor", @"ic"];
      break;
    case PrintFormatElementTypeAlignCenter:
      return @[@"center", @"c"];
      break;
    case PrintFormatElementTypeAlignLeft:
      return @[@"left", @"l"];
      break;
    case PrintFormatElementTypeAlignRight:
      return @[@"right", @"r"];
      break;
    case PrintFormatElementTypeBarcode:
      return @[@"barcode", @"bc"];
    case PrintFormatElementTypeRows:
      return @[@"rows", @"rs"];
    case PrintFormatElementTypeRow:
      return @[@"row", @"r"];
    case PrintFormatElementTypeCut:
      return @[@"cut", @"c"];
    case PrintFormatElementTypeImage:
      return @[@"image", @"img"];
      break;
    default:
      return nil;
      break;
  }
}

- (PrintFormatElementType)elementTypeForName:(NSString *)name
{
  int elements[16] = { PrintFormatElementTypeBold,
    PrintFormatElementTypeTab,
    PrintFormatElementTypeNewline,
    PrintFormatElementTypeAlignCenter,
    PrintFormatElementTypeAlignLeft,
    PrintFormatElementTypeAlignRight,
    PrintFormatElementTypeUnderline,
    PrintFormatElementTypeUpperline,
    PrintFormatElementTypeDashedline,
    PrintFormatElementTypeScale,
    PrintFormatElementTypeInvertColor,
    PrintFormatElementTypeBarcode,
    PrintFormatElementTypeRows,
    PrintFormatElementTypeRow,
    PrintFormatElementTypeCut,
    PrintFormatElementTypeImage,
  };
  
  PrintFormatElementType type = PrintFormatElementTypeUnknown;
  
  for(int x = 0; x < sizeof(elements) / sizeof(int); x++) {
    PrintFormatElementType et = elements[x];
    NSArray *strings = [self elementNamesForFormatterType:et];
    
    if([strings containsObject:[name lowercaseString]]) {
      type = et;
      break;
    }
  }
  
  return type;
}

@end

