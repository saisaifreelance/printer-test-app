//
//  PrinterCommands.h
//  YocoPOS
//
//  Created by Jabulani Mpofu on 23/2/2018.
//  Copyright © 2018 Facebook. All rights reserved.
//

#ifndef PrinterCommands_h
#define PrinterCommands_h


#endif /* PrinterCommands_h */

typedef enum PrinterBarcodeType
{
  PrinterBarcodeTypeUPCE,
  PrinterBarcodeTypeUPCA,
  PrinterBarcodeTypeEAN8,
  PrinterBarcodeTypeEAN13,
  PrinterBarcodeTypeCode39,
  PrinterBarcodeTypeITF,
  PrinterBarcodeTypeCode128,
  PrinterBarcodeTypeCode93,
  PrinterBarcodeTypeNW7
} PrinterBarcodeType;

#define kPrinterCMD_Tab                 @"\x09"
#define kPrinterCMD_Newline             @"\x0A"
