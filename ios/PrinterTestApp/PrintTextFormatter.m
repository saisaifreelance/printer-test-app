//
//  PrintTextFormatter.m
//  PrinterTestApp
//
//  Created by Jabulani Mpofu on 25/2/2018.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PrintTextFormatter.h"

@interface PrintTextFormatter ()

@property (nonatomic, strong) NSMutableData *commands;
@property (nonatomic) ISCBBuilder *builder;
@property (nonatomic) StarIoExtEmulation emulation;
@property (nonatomic) NSStringEncoding encoding;
@property (nonatomic) PaperSizeIndex paperSizeIndex;
@property (nonatomic) int col1;
@property (nonatomic) int col2;

@end

@implementation PrintTextFormatter

#pragma mark - Initialization

+ (PrintTextFormatter *)formatter
{
  StarIoExtEmulation emulation = StarIoExtEmulationStarPRNT;
  PrintTextFormatter *formatter = [[PrintTextFormatter alloc] init:emulation];
  return formatter;
}

+ (PrintTextFormatter *)formatter:(StarIoExtEmulation)emulation
{
  PrintTextFormatter *formatter = [[PrintTextFormatter alloc] init:emulation];
  return formatter;
}

- (id)init:(StarIoExtEmulation)emulation
{
  self = [super init];
  if(self) {
    self.encoding = NSUTF8StringEncoding;
    self.paperSizeIndex = PaperSizeIndexTwoInch;
    self.builder = [StarIoExt createCommandBuilder:emulation];
    [self.builder beginDocument];
    [self.builder appendCodePage:SCBCodePageTypeUTF8];
    [self.builder appendInternational:SCBInternationalTypeUSA];
    [self.builder appendCharacterSpace:0];
 
    /*
    
    
    //[self.builder appendCutPaper:SCBCutPaperActionPartialCutWithFeed];
     */
  }
  return self;
}

- (void)rows:(int)col1 col2:(int)col2
{
  self.col1 = col1;
  self.col2 = col2;
  
}

- (void)row:(NSString *)col1 col2:(NSString *)col2
{
  NSMutableArray *col1Array = [NSMutableArray arrayWithCapacity:1];
  
  if (col1.length > self.col1) {
    NSMutableString *source = [NSMutableString stringWithString:col1];
    while (source.length > 0) {
      int length = source.length > self.col1 ? self.col1 : source.length;
      NSString *section = [source substringToIndex:length];
      [col1Array addObject:section];
      [source deleteCharactersInRange:NSMakeRange(0, length)];
    }
  } else {
    [col1Array addObject:col1];
  }
  
  NSMutableString *rowcol1 = [NSMutableString stringWithString:col1Array[0]];
  for (int i = 1; i < [col1Array count]; i++) {
    [rowcol1 appendString: [NSString stringWithFormat:@"\n%@", col1Array[i]]];
  }
  
  NSMutableString *rowcol2 = [NSMutableString stringWithString:col2];
  while (rowcol2.length < self.col2) {
    [rowcol2 insertString:@" " atIndex:0];
  }
  NSData *otherData = [[NSString stringWithFormat:@"%@\t%@\n", rowcol1, rowcol2] dataUsingEncoding:NSASCIIStringEncoding];
  
  NSArray<NSNumber *> *positions = @[@(self.col1 + 2)];
  [self.builder appendHorizontalTabPosition:positions];
  [self.builder appendData:otherData];
}


- (void)image:(NSString *)url width:(int)width height:(int)height
{
  UIImage *image=[UIImage imageWithContentsOfFile:url];
//  UIImage *image = [UIImage imageNamed:@"EnglishCouponImage.png"];
  [self.builder appendBitmap:image diffusion:NO width:width bothScale:YES rotation:SCBBitmapConverterRotationNormal];
  
}

#pragma mark - Commands

- (void)cut:(BOOL) full
{
  if (full) {
    [_builder appendCutPaper:SCBCutPaperActionPartialCut];
    return;
  }
  [_builder appendCutPaper:SCBCutPaperActionFullCut];
}

- (void)cutWithFeed:(BOOL) full
{
  if (full) {
    [_builder appendCutPaper:SCBCutPaperActionPartialCutWithFeed];
    return;
  }
  [_builder appendCutPaper:SCBCutPaperActionFullCutWithFeed];
}

- (void)tab
{
  [self add:kPrinterCMD_Tab];
}

- (void)newline
{
  [self.builder appendLineFeed];
}

- (void)dashedNewLine
{
  [self add:@"\r\n--------------------------------\r\n"];
}

#pragma mark - Text Formatting

- (void)bold:(NSString *)text next:(PrintTextFormatterBlock)block
{
  [self.builder appendDataWithEmphasis:[text dataUsingEncoding:self.encoding]];
  if(block) {
    block(text);
  }
}

- (void)bold:(BOOL)start
{
  [self.builder appendEmphasis:start];
}

- (void)underline:(NSString *)text next:(PrintTextFormatterBlock)block
{
  [self.builder appendDataWithUnderLine:[text dataUsingEncoding:self.encoding]];
  if(block) {
    block(text);
  }
}

- (void)underline:(BOOL)start
{
  [self.builder appendUnderLine:start];
}

- (void)upperline:(NSString *)text next:(PrintTextFormatterBlock)block
{
  [self.builder appendDataWithUnderLine:[text dataUsingEncoding:self.encoding]];
  if(block) {
    block(text);
  }
}

- (void)upperline:(BOOL)start
{
  [self.builder appendUnderLine:start];
}

- (void)scale:(NSString *)text width:(int)width height:(int)height next:(PrintTextFormatterBlock)block
{
  [self.builder appendDataWithMultiple:[text dataUsingEncoding:self.encoding] width:width height:height];
  if(block) {
    block(text);
  }
}

- (void)scale:(BOOL)start width:(int)width height:(int)height
{
  if (start) {
    [self.builder appendMultiple:width height:height];
  }
  [self.builder appendMultiple:1 height:1];
}

- (void)invert:(NSString *)text next:(PrintTextFormatterBlock)block
{
  [self.builder appendDataWithInvert:[text dataUsingEncoding:self.encoding]];
  if(block) {
    block(text);
  }
}

- (void)invert:(BOOL)start
{
  [self.builder appendInvert:start];
}

- (void)invertColor:(NSString *)text next:(PrintTextFormatterBlock)block
{
  [self.builder appendDataWithInvert:[text dataUsingEncoding:self.encoding]];
  if(block) {
    block(text);
  }
}

- (void)invertColor:(BOOL)start
{
  [self.builder appendInvert:start];
}


#pragma mark - Text Alignment

- (void)alignLeft
{
  [self.builder appendAlignment:SCBAlignmentPositionLeft];
}

- (void)alignRight;
{
  [self.builder appendAlignment:SCBAlignmentPositionRight];
}

- (void)alignCenter;
{
  [self.builder appendAlignment:SCBAlignmentPositionCenter];
}


#pragma mark - Barcodes

- (void)barcode:(PrinterBarcodeType)type text:(NSString *)text
{
  [self.builder appendBarcodeData:[[NSString stringWithFormat:@"{B%@", text] dataUsingEncoding:NSASCIIStringEncoding] symbology:SCBBarcodeSymbologyCode128 width:SCBBarcodeWidthMode2 height:40 hri:YES];
//  [self.builder appendBarcodeData:[text dataUsingEncoding:NSASCIIStringEncoding] symbology:SCBBarcodeSymbologyCode128 width:SCBBarcodeWidthMode2 height:40 hri:YES];
}

#pragma mark - Helpers

- (NSData *)formattedData
{
//  [self createRasterReceiptData];
  [self.builder endDocument];
  _commands = [self.builder.commands copy];
  return _commands;
}

- (void)createRasterReceiptData
{
  
  UIImage *image = [self createRasterReceiptImage];
  
  [self.builder appendBitmap:image diffusion:NO];
  
  [self.builder appendCutPaper:SCBCutPaperActionPartialCutWithFeed];
  
  [self.builder appendPeripheral:SCBPeripheralChannelNo1];
}

- (UIImage *)createRasterReceiptImage {
  UIImage *image;
  
  switch (_paperSizeIndex) {
    default                    :
      //      case PaperSizeIndexTwoInch :
      image = [self create2inchRasterReceiptImage];
      break;
    case PaperSizeIndexThreeInch :
      image = [self create3inchRasterReceiptImage];
      break;
    case PaperSizeIndexFourInch :
      image = [self create4inchRasterReceiptImage];
      break;
    case PaperSizeIndexDotImpactThreeInch :
      image = nil;
      break;
  }
  
  return image;
}

- (UIImage *)create2inchRasterReceiptImage {
  NSString *textToPrint =
  @"   Star Clothing Boutique\n"
  "        123 Star Road\n"
  "      City, State 12345\n"
  "\n"
  "Date:MM/DD/YYYY Time:HH:MM PM\n"
  "-----------------------------\n"
  "SALE\n"
  "SKU       Description   Total\n"
  "300678566 PLAIN T-SHIRT 10.99\n"
  "300692003 BLACK DENIM   29.99\n"
  "300651148 BLUE DENIM    29.99\n"
  "300642980 STRIPED DRESS 49.99\n"
  "30063847  BLACK BOOTS   35.99\n"
  "\n"
  "Subtotal               156.95\n"
  "Tax                      0.00\n"
  "-----------------------------\n"
  "Total                 $156.95\n"
  "-----------------------------\n"
  "\n"
  "Charge\n"
  "159.95\n"
  "Visa XXXX-XXXX-XXXX-0123\n"
  "Refunds and Exchanges\n"
  "Within 30 days with receipt\n"
  "And tags attached\n";
  
  UIFont *font = [UIFont fontWithName:@"Menlo" size:10 * 2];
  //  UIFont *font = [UIFont fontWithName:@"Menlo" size:11 * 2];
  //  UIFont *font = [UIFont fontWithName:@"Menlo" size:12 * 2];
  
  return [self imageWithString:textToPrint font:font width:384];     // 2inch(384dots)
}

- (UIImage *)create3inchRasterReceiptImage {
  NSString *textToPrint =
  @"        Star Clothing Boutique\n"
  "             123 Star Road\n"
  "           City, State 12345\n"
  "\n"
  "Date:MM/DD/YYYY          Time:HH:MM PM\n"
  "--------------------------------------\n"
  "SALE\n"
  "SKU            Description       Total\n"
  "300678566      PLAIN T-SHIRT     10.99\n"
  "300692003      BLACK DENIM       29.99\n"
  "300651148      BLUE DENIM        29.99\n"
  "300642980      STRIPED DRESS     49.99\n"
  "30063847       BLACK BOOTS       35.99\n"
  "\n"
  "Subtotal                        156.95\n"
  "Tax                               0.00\n"
  "--------------------------------------\n"
  "Total                          $156.95\n"
  "--------------------------------------\n"
  "\n"
  "Charge\n"
  "159.95\n"
  "Visa XXXX-XXXX-XXXX-0123\n"
  "Refunds and Exchanges\n"
  "Within 30 days with receipt\n"
  "And tags attached\n";
  
  UIFont *font = [UIFont fontWithName:@"Menlo" size:12 * 2];
  
  return [self imageWithString:textToPrint font:font width:576];     // 3inch(576dots)
}

- (UIImage *)create4inchRasterReceiptImage {
  NSString *textToPrint =
  @"                   Star Clothing Boutique\n"
  "                        123 Star Road\n"
  "                      City, State 12345\n"
  "\n"
  "Date:MM/DD/YYYY                             Time:HH:MM PM\n"
  "---------------------------------------------------------\n"
  "SALE\n"
  "SKU                     Description                 Total\n"
  "300678566               PLAIN T-SHIRT               10.99\n"
  "300692003               BLACK DENIM                 29.99\n"
  "300651148               BLUE DENIM                  29.99\n"
  "300642980               STRIPED DRESS               49.99\n"
  "300638471               BLACK BOOTS                 35.99\n"
  "\n"
  "Subtotal                                           156.95\n"
  "Tax                                                  0.00\n"
  "---------------------------------------------------------\n"
  "Total                                             $156.95\n"
  "---------------------------------------------------------\n"
  "\n"
  "Charge\n"
  "159.95\n"
  "Visa XXXX-XXXX-XXXX-0123\n"
  "Refunds and Exchanges\n"
  "Within 30 days with receipt\n"
  "And tags attached\n";
  
  UIFont *font = [UIFont fontWithName:@"Menlo" size:12 * 2];
  
  return [self imageWithString:textToPrint font:font width:832];     // 4inch(832dots)
}

- (UIImage *)imageWithString:(NSString *)string font:(UIFont *)font width:(CGFloat)width {
  NSDictionary *attributeDic = @{NSFontAttributeName:font};
  
  CGSize size = [string boundingRectWithSize:CGSizeMake(width, 10000)
                                     options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingTruncatesLastVisibleLine
                                  attributes:attributeDic
                                     context:nil].size;
  
  if ([UIScreen.mainScreen respondsToSelector:@selector(scale)]) {
    if (UIScreen.mainScreen.scale == 2.0) {
      UIGraphicsBeginImageContextWithOptions(size, NO, 1.0);
    } else {
      UIGraphicsBeginImageContext(size);
    }
  } else {
    UIGraphicsBeginImageContext(size);
  }
  
  CGContextRef context = UIGraphicsGetCurrentContext();
  
  [[UIColor whiteColor] set];
  
  CGRect rect = CGRectMake(0, 0, size.width + 1, size.height + 1);
  
  CGContextFillRect(context, rect);
  
  NSDictionary *attributes = @ {
  NSForegroundColorAttributeName:[UIColor blackColor],
  NSFontAttributeName:font
  };
  
  [string drawInRect:rect withAttributes:attributes];
  
  UIImage *imageToPrint = UIGraphicsGetImageFromCurrentImageContext();
  
  UIGraphicsEndImageContext();
  
  return imageToPrint;
}

- (void)add:(NSString *)text
{
  [self.builder appendData:[text dataUsingEncoding:self.encoding]];
//  [_commands appendData:[text dataUsingEncoding:NSASCIIStringEncoding]];
}

@end
