//
//  Printable.m
//  PrinterTestApp
//
//  Created by Jabulani Mpofu on 25/2/2018.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import "Printable.h"

@implementation NSObject (Printable)

- (void)print
{
  [self print:[Printer connectedPrinter]];
}

- (void)print:(Printer *)printer
{
  [printer print:[self performSelector:@selector(printedFormat)]];
}

@end
