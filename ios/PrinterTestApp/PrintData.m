//
//  PrintData.m
//  PrinterTestApp
//
//  Created by Jabulani Mpofu on 24/2/2018.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "PrintData.h"

@implementation PrintData

- (id)initWithDictionary:(NSDictionary *)dictionary atFilePath:(NSString *)filePath
{
  self = [super init];
  
  if(self) {
    self.dictionary = dictionary;
    self.filePath = filePath;
  }
  
  return self;
}

@end
