//
//  Printer.h
//  PrinterTestApp
//
//  Created by Jabulani Mpofu on 23/2/2018.
//  Copyright © 2018 Facebook. All rights reserved.
//

#ifndef Printer_h
#define Printer_h


#endif /* Printer_h */

#import <Foundation/Foundation.h>
#import <StarIO/SMPort.h>
#import <StarIO_Extension/StarIoExt.h>
#import "PrintData.h"

#define kConnectedPrinterKey    @"ConnectedPrinterKey"

typedef enum PrinterStatus
{
  PrinterStatusDisconnected,
  PrinterStatusConnecting,
  PrinterStatusConnected,
  PrinterStatusLowPaper,
  PrinterStatusCoverOpen,
  PrinterStatusOutOfPaper,
  PrinterStatusConnectionError,
  PrinterStatusLostConnectionError,
  PrinterStatusPrintError,
  PrinterStatusUnknownError,
  PrinterStatusIncompatible,
  PrinterStatusNoStatus
} PrinterStatus;

typedef void(^PrinterResultBlock)(BOOL success);
typedef void(^PrinterSearchBlock)(NSArray *found);

@class Printer;
@protocol PrinterDelegate <NSObject>

@required
- (void)printer:(Printer *)printer didChangeStatus:(PrinterStatus)status;

@end

@class PortInfo, Printable;

@interface Printer : NSObject

@property (nonatomic, weak) id<PrinterDelegate> delegate;
@property (nonatomic, readwrite) PrinterStatus status;
@property (nonatomic, strong) SMPort *port;

@property (nonatomic, strong) NSString *modelName;
@property (nonatomic, strong) NSString *portName;
@property (nonatomic, strong) NSString *macAddress;
@property (nonatomic, strong) NSString *friendlyName;
@property (nonatomic) NSString *portSettings;
@property (nonatomic) StarIoExtEmulation emulation;

// Helper method
// Returns `friendlyName` if it exists, else `modelName`
@property (nonatomic, readonly) NSString *name;

@property (nonatomic, readonly) BOOL isReadyToPrint;
@property (nonatomic, readonly) BOOL hasError;
@property (nonatomic, readonly) BOOL isOffline;
@property (nonatomic, readonly) BOOL isOnlineWithError;
@property (nonatomic, readonly) BOOL isCompatible;

+ (Printer *)printerFromPort:(PortInfo *)port;
+ (Printer *)printerFromDict:(NSDictionary *)printerData;
+ (Printer *)connectedPrinter;
+ (void)search:(PrinterSearchBlock)block;
+ (Class)portClass;
+ (NSString *)stringForStatus:(PrinterStatus)status;

- (void)connect:(PrinterResultBlock)result;
- (void)disconnect;
- (void)printTest;
- (NSDictionary *)dictionaryValue;

// Should only be called by unit tests
@property (nonatomic, strong) NSMutableArray *jobs;
@property (nonatomic, strong) NSOperationQueue *queue;
- (void)startHeartbeat;
- (void)stopHeartbeat;

// @todo ideally these should usually not be called directly, rather objects should
// conform to the `Printable` protocol
- (void)print:(PrintData *)data;
- (void)printContent:(NSDictionary *)dictionary xml:(NSMutableString *)s;
- (void)printRaw:(NSData *)data;

@end
