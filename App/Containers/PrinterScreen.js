import React, { Component } from 'react'
import {View, ScrollView, Alert, Image} from 'react-native'
import { format } from 'currency-formatter'
import ImagePicker from 'react-native-image-crop-picker'
import {CachedImage, ImageCache} from "react-native-img-cache"

import { List, ListItem, Button, MainHeader, ProgressModal } from '../Components'
import colors from '../Components/config/colors'

import { NativeModules } from 'react-native';
const ReceiptPrinter = NativeModules.ReceiptPrinter;

// Styles
import styles from './Styles/PrinterScreenStyle'

// const uri = "https://beebom-redkapmedia.netdna-ssl.com/wp-content/uploads/2016/01/Reverse-Image-Search-Engines-Apps-And-Its-Uses-2016.jpg"
const uri = "https://alpha-code.s3.amazonaws.com/uploads/production/logo_image/206/logo_Alphacode_Partner_Yoco_500.png"

const cart = {
  total: 88,
  items: [
    {
      title: 'Black panther tickets',
      qty: 2,
      price: 13.0,
      total: 26.0
    },
    {
      title: 'Taxi',
      qty: 1,
      price: 11.0,
      total: 11.0
    },
    {
      title: 'Popcorn',
      qty: 1,
      price: 1.0,
      total: 1.0
    },
  ],
  addOns: [
    {
      title: 'VAT',
      qty: 1,
      price: 3.0
    },
  ],
  deductions: [
    {
      title: 'Coupon',
      qty: 1,
      price: 4.0
    },
  ],
  total: 89,
}

const dictionary = {
  "{{option1}}": "|Var A|",
  "{{option2}}": "|Var B|"
}

class PrintScreen extends Component {
  state = {
    progress: false,
    printers: null,
    currentPrinter: null,
    data: [],
    receipt: false,
    image: {
      local:true,
      uri: null,
      path: null,
    },
    meta: {
      _title: `Printers`,
      _leftTitle: 'connect',
      _leftSubTitle: 'Search',
      _listTitle: 'Nearby Printers',
      _button: 'Search'
    }
  }

  observer = (path) => {
      const {local, uri, path: currentPath} = this.state.image
      console.log(`?????????????????????path of the image in the cache:\n ${path}`);
      if (local) {
      console.log(`?????????????????????is is local and so we return`);
        return
      }
      const image = {
        local,
        uri,
        path,
      }
      this.setState({image})
  }

  selectImageType = () => {
    Alert.alert(
      'Select image location',
      'Where would you like to get the image?',
      [
        {text: 'Cancel', onPress: () => {}, style: 'cancel'},
        {text: 'Enter URL', onPress: () => {
          ImageCache.get().on({ uri }, this.observer)
          this.setState({image: {local: false, uri, path: null}})
        }},
        {text: 'Select from Library', onPress: this.selectImage},
        {text: 'Take Photo', onPress: () => {}},
      ],
      { cancelable: true }
    )
  }

  selectImage = () => {
    return ImagePicker
      .openPicker({
        width: 200,
        height: 200,
        cropping: true
      })
    .then(image => {
      console.log('seccuess picking')
      console.log(image);
      let path = image.path
      if (path.indexOf('/private') === 0) {
        path = path.slice(8)
      }
      this.setState({image: {local: true, uri: path, path}})
    })
    .catch((e) => {
      console.log('error picking')
      console.log(e)
    })
  }

  searchForPrinters = () => {
    this.setState({ progress: 'searching for nearby printers...'})
    return ReceiptPrinter
      .search()
      .then((printers) => {
        console.log('printers')
        console.log(printers)
        if (!Array.isArray(printers) || printers.length < 1) {
          throw new Error('No printers found!')
        }
        this.setPrinterData(printers, { printers, progress: null, currentPrinter: null })
      })
      .catch((e) => {
        console.log('error')
        console.log(e)
        this.setPrinterData([], { printers: [], progress: null, error: e.message, currentPrinter: null })
      })
  }

  setConnectedPrinter = (currentPrinter) => {
    console.log('>>>>>> CONNECTING TO PRINTER')
    this.setState({progress: 'Connecting to printer...', error: null})
    return ReceiptPrinter
      .setConnectedPrinter(currentPrinter, true)
      .then((response) => {
        console.log('setConnectedPrinter: response')
        console.log(response)
        console.log('>>>>>CONECTED')
        this.setReceiptData(cart, currentPrinter, {progress: null, error: null})
      })
      .catch((e) => {
        console.log('setConnectedPrinter: e')
        console.log(e)
        Alert.alert(
          'Failed to connect to printer.',
          'Try again?',
          [
            {text: 'No', onPress: () => this.setState({ error: null, progress: null }), style: 'cancel'},
            {text: 'Yes', onPress: () => this.setConnectedPrinter(currentPrinter)},
          ],
          { cancelable: false }
        )
        this.setPrinterData([], { printers: [], progress: null, error: e.message, currentPrinter: null })
      })
  }

  printFromTemplate = () => {
    const xml = `
    <print>
        <center/>
        <text><scale scale="2">Star Clothing Boutique\n</scale></text>
        <text><bold>123 Star Road\n</bold></text>
        <text>City, State 12345\n</text>
        <image source="${this.state.image.path}" width="300" height="200" />
        <newline />
        <left />
        <rows col1="15" col2="14">
            <row col1="Date:MM/DD/YYYY" col2="Time:HH:MM PM" />
        </rows>
        <dashednewline />
        <text><scale scale="2"><bold>SALE\n</bold></scale></text>
        <rows col1="22" col2="7">
            <row col1="Description" col2="Total" />
            ${cart.items.map((item, i) => `<row col1=".${item.title} x ${item.qty}" col2="${format(item.total, { code: 'USD' })}" />`)}
            ${cart.deductions.map((item, i) => `<row col1="${item.title} x ${item.qty}" col2="-${format(item.price, { code: 'USD' })}" />`)}
            ${cart.addOns.map((item, i) => `<row col1="${item.title} x ${item.qty}" col2="${format(item.price, { code: 'USD' })}" />`)}
            <row col1="Apple" col2="$2.00" />
        </rows>
        <rows col1="22" col2="7">
            <row col1="total" col2="${format(cart.total, { code: 'USD' })}" />
        </rows>
        <center />
        <barcode>1121123456.</barcode>
    </print>
    `
    return ReceiptPrinter
      .print(dictionary, xml)
      .then((...args) => {
        console.log('RESPONSE')
        console.log(args)
      })
      .catch((e) => {
        console.log('RESPONSE ERROR')
        console.log(e)
      })
  }

  setPrinterData = (printers, other = {}) => {
    const data = printers.map((printer) => {
      return {
        key: printer.macAddress,
        title: printer.modelName,
        leftIconContainerColor: colors.purple,
        leftIcon: {name: 'print'},
        subtitle: printer.portName,
        rightTitle: 'select',
        billing: true,
        rightTitleStyle: {fontWeight: '500'},
        onPress: () => {
          Alert.alert(
            'This is your pinter?',
            `${printer.modelName}\n${printer.modelName}`,
            [
              {text: 'No', onPress: () => {}, style: 'cancel'},
              {text: 'Yes', onPress: () => this.setConnectedPrinter(printer)},
            ],
            { cancelable: true }
          )
        }
      }
    })

    const meta = {
      _title: `Printers`,
      _leftTitle: 'connect',
      _leftSubTitle: 'Search',
      _listTitle: 'Nearby Printers',
      _button: 'Search'
    }

    this.setState({...other, meta, data});
  }

  setReceiptData = (cart, currentPrinter, other = {}) => {
    console.log('set recipts data:')
    console.log(cart)
    const data = cart.items.map((item, key) => {
      console.log('set recipts data: item')
      console.log(item)
      const {
        title,
        qty,
        price,
        total
      } = item

      return {
        key,
        title: title,
        leftIconContainerColor: colors.purple,
        leftIcon: {name: 'circle'},
        subtitle: `${format(price, { code: 'USD' })} x ${qty}`,
        rightTitle: `${format(price, { code: 'USD' })} x ${qty}`,
        billing: true,
        rightTitleStyle: {fontWeight: '500'}
      }
    })

    const meta = {
      _title: `Printer: ${currentPrinter.modelName}`,
      _leftTitle: `Printer: ${currentPrinter.modelName}`,
      _leftSubTitle: 'Sample Receipt',
      _listTitle: 'Cart Items',
      _button: 'Print Receipt'
    }

    console.log('set recipts data: data')
    console.log(data)
    this.setState({...other, currentPrinter, meta, data});
  }

  onSubmit = () => {
    if (this.state.currentPrinter) {
      this.printFromTemplate();
    } else {
      //search
      this.searchForPrinters()
    }
  }

  backAction = () => {

  }

  componentDidMount () {
  }

  componentDidUpdate (prevProps, prevState) {
    const { local, uri, path } = this.state.image
    if (local && !prevState.image.local) {
      if (local) {
        ImageCache.get().dispose({ uri }, this.observer)
      }
    }
  }

  render () {
    const {
      data,
      image
    } = this.state
    console.log('>>>>>>>>>>>>>>>>>>this.state')
    console.log(this.state)

    let _progress = null
    let _items = null
    let _image = require('../Images/balance.png')
    let ImageComponent = Image

    if (image.uri && image.local) {
      ImageComponent = Image
      _image = { uri: image.uri }
    } else if (image.uri) {
      ImageComponent = CachedImage
      _image = { uri: image.uri }
    }

    if (data) {
      _items = Object.keys(data).map((key, index) => {
        const props = data[key]
        return (
          <ListItem
            {...props}
          />
        )
      })
    }

    if (this.state.progress) {
      _progress = (<ProgressModal message={this.state.progress} />)
    }

    return (
      <View style={{backgroundColor: '#D3DFEF', flex: 1, paddingBottom: 15}}>
        <ScrollView style={{flex: 1}}>
          <MainHeader
            ImageComponent={ImageComponent}
            onPressImage={this.selectImageType}
            onLeftIconPressed={this.backAction}
            heading={this.state.meta._title}
            titleBalance={this.state.meta._leftTitle}
            valueBalance={this.state.meta._leftSubTitle}
            cardTitle={this.state.meta._listTitle}
            image={_image}
            imageSize={{width: 55, height: 55}}
          />
          <List>
            {_items}
          </List>
        </ScrollView>
        <View>
          <Button
            onPress={this.onSubmit}
            buttonStyle={{marginTop: 30}}
            title={this.state.meta._button}
          />
        </View>
        {_progress}
      </View>
    )
  }
}

export default PrintScreen
