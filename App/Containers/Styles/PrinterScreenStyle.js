import { StyleSheet } from 'react-native'
import { ApplicationStyles, Metrics } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  imageContainer: {
    marginLeft: 15,
    marginRight: 15,
    alignItems: 'center'
  },
  image: {
    width: Metrics.screenWidth - 30,
    height: 200
  },
})
