import { StackNavigator } from 'react-navigation'
import PrinterScreen from '../Containers/PrinterScreen'

import styles from './Styles/NavigationStyles'

// Manifest of possible screens
const PrimaryNav = StackNavigator({
  PrinterScreen: { screen: PrinterScreen }
}, {
  // Default config for all screens
  headerMode: 'none',
  initialRouteName: 'PrinterScreen',
  navigationOptions: {
    headerStyle: styles.header
  }
})

export default PrimaryNav
