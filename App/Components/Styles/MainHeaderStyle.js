import { StyleSheet } from 'react-native'
import colors from '../config/colors'

export default StyleSheet.create({
  container: {
    flex: 1
  },
  heading: {
    color: '#24272B',
    fontWeight: '500',
    fontSize: 22,
    marginLeft: 10,
    justifyContent: 'center',
  },
  headingLetter: {
    color: '#fff',
    fontWeight: '500',
    fontSize: 14,
  },
  headingIcon: {
    width: 40,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    display: 'flex',
    borderRadius: 33,
    backgroundColor: colors.yellow,
  },
  hero: {
    flexDirection: 'column',
  },
  topLine: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: 15,
    backgroundColor: colors.white,
  },
  topText: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  titleContainer: {},
  button: {
    marginTop: 15,
  },
  title: {
    textAlign: 'center',
    color: '#5e6977'
  },
  socialRow: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  balanceField: {
    height: 120,
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: '#E9EFF7',
    borderTopWidth: 1,
    padding: 15,
    backgroundColor: colors.white,
  },
  total: {
    color: '#136EF1',
    fontSize: 28,
    fontWeight: '500',
    marginTop: 10,
  },
  myCard: {
    flexDirection: 'row',
    backgroundColor: '#F2F5FB',
    height: 55,
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 15,
  },
  cardTitle: {
    fontSize: 15,
    fontWeight: '500',
    color: '#3E4A59',
    opacity: 0.7,
  },
  balance: {
    flexDirection: 'row',
    height: 80,
    backgroundColor: '#fff',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderColor: '#E9EFF7',
    paddingTop: 10,
    borderTopWidth: 1,
  },
  balanceTitle: {
    color: '#3E4A59',
    opacity: 0.7,
    fontSize: 12,
  },
  balanceTitle2: {
    color: '#3E4A59',
    opacity: 0.7,
    fontSize: 12,
    textAlign: 'right',
  },
  expenses: {
    borderRightWidth: 1,
    borderColor: '#E9EFF7',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    height: 90,
  },
  balanceLine: {
    flexDirection: 'row',
  },
  income: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 80,
  },
  expensesAmount: {
    color: '#24272B',
    fontSize: 20,
    fontWeight: '500',
  },
  incomeAmount: {
    color: '#24272B',
    fontSize: 20,
    fontWeight: '500',
  }
})
