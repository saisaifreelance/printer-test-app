import { StyleSheet } from 'react-native'
import { Metrics, Colors, Fonts } from '../../Themes'
const maxProgressWidth = 300
const maxAllowableProgressWidth = Metrics.screenWidth - 32

export default StyleSheet.create({
  container: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(0,0,0,0.5)'
  },
  content: {
    backgroundColor: Colors.snow,
    padding: Metrics.doubleBaseMargin,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 8,
    width: maxProgressWidth < maxAllowableProgressWidth ? maxProgressWidth : maxAllowableProgressWidth
  },
  message: {
    marginTop: Metrics.doubleBaseMargin,
    ...Fonts.style.h5,
    color: Colors.blue,
    textAlign: 'center'
  }
})
