import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { StyleSheet, View, Text, Image, TouchableOpacity } from 'react-native'
import styles from './Styles/MainHeaderStyle'

import Icon from './Icon'

export default class MainHeader extends Component {
  // Prop type warnings
  static propTypes = {
    onLeftIconPressed: PropTypes.func,
    onRightIconPressed: PropTypes.func,
    headingIcon: PropTypes.string,
    heading: PropTypes.string,
    valueExpenses: PropTypes.string,
    valueIncome: PropTypes.string,
    titleBalance: PropTypes.string,
    valueBalance: PropTypes.string,
    cardTitle: PropTypes.string,
    onPressImage: PropTypes.func,
    ImageComponent: PropTypes.func,
  }

  // Defaults for props
  static defaultProps = {
    onLeftIconPressed: null,
    onRightIconPressed: null,
    onPressImage: null,
    headingIcon: '',
    heading: '',
    valueExpenses: '',
    valueIncome: '',
    titleBalance: 'Total',
    valueBalance: '',
    cardTitle: '',
    image: require('../Images/balance.png'),
    imageSize: {width: 55, height: 55, justifyContent: 'center'},
    ImageComponent: Image
  }

  render () {
    const {
      headingIcon,
      heading,
      image,
      imageSize,
      onLeftIconPressed,
      onRightIconPressed,
      titleBalance,
      valueBalance,
      valueExpenses,
      valueIncome,
      cardTitle,
      onPressImage,
      ImageComponent
    } = this.props
    return (
      <View>
        <View style={styles.hero}>
          <View style={styles.topLine}>
            <Icon
              onPress={onLeftIconPressed}
              color='#B3BFD0'
              name='times-circle'
              size={20}
              marginLeft={10}
            />
            <View style={styles.topText}>
              <Text style={styles.heading}>{heading}</Text>
            </View>
            <Icon
              onPress={onRightIconPressed}
              color='#B3BFD0'
              name='chevron-right'
              size={18}
              marginRight={10}
            />
          </View>
          <View style={styles.balanceField}>
            <TouchableOpacity onPress={onPressImage}>
            <ImageComponent
              style={[MainHeader.defaultProps.imageSize, imageSize && imageSize]}
              source={image}
            />
            </TouchableOpacity>
            <View style={styles.balanceTotal}>
              <Text style={styles.balanceTitle2}>{titleBalance}</Text>
              <Text style={styles.total}>{valueBalance}</Text>
            </View>
          </View>
        </View>

        <View style={styles.myCard}>
          <Text style={styles.cardTitle}>{cardTitle}</Text>
        </View>
      </View>
    )
  }
}
